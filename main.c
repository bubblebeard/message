#include <linux/ipc.h>
#include <linux/msg.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>

#define TAKE 5
#define GOLD 50
#define LOOP 1
#define REQ 1
#define TERM -1

typedef struct msg {
    long mtype;
    int data;
} mymsg;

int writer(int, int);
int reader(int, int, int, int*);

int main(int argc, char** argv) {
    int msg;
    msg = msgget(IPC_PRIVATE, IPC_CREAT | 0664);
    if (msg == -1) {
        printf("main: unable to create queue\n");
        return (EXIT_FAILURE);
    }

    int orcs = atoi(argv[1]);

    int id[orcs];
    int gold = 40;
    pid_t childpid;

    for (int i = 0; i < orcs; i++) {
        if ((childpid = fork()) == -1) {
            perror("fork");
            exit(1);
        }
        if (childpid == 0) {
            writer(msg, i + 100);
            exit(0);
        } else {
            id[i] = i + 100;
        }
    }
    reader(msg, orcs, gold, id);
    return (0);
}

int writer(int msg, int id) {
    mymsg *snd, *rcv;
    rcv = malloc(sizeof (mymsg));
    snd = malloc(sizeof (mymsg));
    snd->mtype = id;
    snd->data = REQ;
    while (LOOP) {
        msgsnd(msg, snd, sizeof (int), 0);
        if (msgrcv(msg, rcv, sizeof (int), id + 300, IPC_NOWAIT) > 0) {
            if (rcv->data == TERM || rcv->data == 0) {
                printf("orc%d: terminated\n", id);
                fflush(stdout);
                return 1;
            }
            if (rcv->data > 0) {
                sleep(rand() % 5);
            } else {
                printf("orc%d: unknown message from parent\n", id);
                fflush(stdout);
            }
        }
    }
    return 1;
}

int reader(int msg, int orcs, int gold, int *id) {
    int cur = 0;
    mymsg *snd, *rcv;
    snd = malloc(sizeof (mymsg));
    rcv = malloc(sizeof (mymsg));
    while (LOOP) {
        if (gold <= 0) {
            printf("parent: no gold left\n");
            fflush(stdout);
            snd->data = TERM;
            for (int i = 0; i < orcs; i++) {
                snd->mtype = id[i] + 300;
                msgsnd(msg, snd, sizeof (snd), 0);
            }
            printf("parent terminated\n");
            fflush(stdout);
            return (EXIT_SUCCESS);
        }
        if (msgrcv(msg, rcv, sizeof (int), id[cur], IPC_NOWAIT) != -1) {
            if (rcv->data == REQ) {
                snd->mtype = id[cur] + 300;
                if (gold < TAKE) {
                    printf("parent: orc%d takes %d gold. 0 gold left\n", id[cur], gold);
                    fflush(stdout);
                    gold = 0;
                    snd->data = gold;
                    msgsnd(msg, snd, sizeof (snd), 0);
                } else {
                    gold = gold - TAKE;
                    printf("parent: orc%d takes %d gold. %d gold left\n", id[cur], TAKE, gold);
                    fflush(stdout);
                    snd->data = gold;
                    msgsnd(msg, snd, sizeof (snd), 0);
                }
            } else {
                printf("parent: unknown message from orc%d\n", id[cur]);
                fflush(stdout);
            }
        }
        if (cur == orcs - 1) {
            cur = 0;
        } else {
            cur++;
        }
    }
}